import {ApplicationService, IApplication} from '@themost/common';
import {DataContext} from '@themost/data';
import DocumentNumberSeriesItem from './models/DocumentNumberSeriesItem';


export declare abstract class DocumentNumberService extends ApplicationService  {
    constructor(app: IApplication);

    abstract next(): any;

    abstract async add(context: DataContext, file: string, item: DocumentNumberSeriesItem): any;

    abstract async replace(context: DataContext, file: string, item: DocumentNumberSeriesItem): any;

    async addFrom(context: DataContext, data: Blob | Buffer, item: DocumentNumberSeriesItem): any;

    async replaceFrom(context: DataContext, data: Blob | Buffer, item: DocumentNumberSeriesItem): any;
    
    getContentType(extname: string): string;
}