import {ApplicationService, AbstractClassError, AbstractMethodError, ArgumentError} from '@themost/common';
// eslint-disable-next-line no-unused-vars
import { DataContext } from '@themost/data';
import {MEDIA_TYPES} from './mediaTypes';
import path from 'path';
import fs from 'fs';
import tmp from 'tmp';
/**
 * @abstract
 * An application service for managing document series
 */
class DocumentNumberService extends ApplicationService {
    constructor(app) {
        super(app);
        if (this.constructor.name === 'DocumentNumberService') {
            throw new AbstractClassError();
        }
    }

    /**
     * @abstract
     */
    next() {
        throw new AbstractMethodError();
    }

    /**
     * Adds a document in a given document series
     * @abstract
     * @param {DataContext} context The current context
     * @param {string} file The full path of a file that is going to be added in the specified document series
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached to a document series item
     */
    // eslint-disable-next-line no-unused-vars
    async add(context, file, attributes) {
        throw new AbstractMethodError();
    }

    /**
     * Replaces a document that exists in a document number series
     * @abstract
     * @param {DataContext} context The current context
     * @param {string} file The full path of a file that is going to be added in the specified document series
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached to a document series item
     */
    // eslint-disable-next-line no-unused-vars
    async replace(context, file, attributes) {
        throw new AbstractMethodError();
    }

   /**
     * Adds a blob in a given document series
     * @param {DataContext} context The current context
     * @param {Blob | Buffer} data The data that is going to be added in the specified document series
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached to a document series item
     */
    async addFrom(context, data, attributes) {
        const self = this;
        let finalData;
        if (data instanceof ArrayBuffer) {
            finalData = new Uint8Array(data)
        } else if (data instanceof Buffer) {
            finalData = data;
        }
        if (finalData == null) {
            throw new ArgumentError('Invalid input. Expected an instance of Buffer or Blob.', 'E_ARG');
        }
        return await new Promise((resolve, reject) => {
            let extname;
            // get extension from name e.g. LoremIpsum.pdf
            if (attributes.name) {
                extname  = path.extname(attributes.name);
            } else if (attributes.contentType) {
                // get extension by contentType
                extname = [...MEDIA_TYPES.entries()]
                .filter(({ 1: v }) => v === attributes.contentType)
                .map(([k]) => k)[0];
            }
            if (extname == null) {
                return reject(new ArgumentError('Either name or contentType is empty or the specified content type is not acceptable.'));
            }
            // eslint-disable-next-line no-unused-vars
            return tmp.file({ postfix: extname, discardDescriptor: true }, (err, file, fd, cleanupCallback) => {
                if (err) {
                    return reject(err);
                }
                return fs.writeFile(file, finalData, err => {
                    if (err) {
                        return reject(err);
                    }
                    self.add(context, file, attributes).then( result => {
                        // cleanup
                        cleanupCallback();
                        // return data
                        return resolve(result);
                    }).catch (err => {
                        return reject(err);
                    });
                });
            });
        });
    }

    /**
     * Replaces a document which exists in a document series with the given blob
     * @param {DataContext} context The current context
     * @param {Blob | Buffer| ArrayBuffer} data The data that is going to be added in the specified document series
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached to a document series item
     */
    async replaceFrom(context, data, attributes) {
        const self = this;
        let finalData;
        if (data instanceof ArrayBuffer) {
            finalData = new Uint8Array(data)
        } else if (data instanceof Buffer) {
            finalData = data;
        }
        if (finalData == null) {
            throw new ArgumentError('Invalid input. Expected an instance of Buffer or Blob.', 'E_ARG');
        }
        return await new Promise((resolve, reject) => {
            let extname;
            // get extension from name e.g. LoremIpsum.pdf
            if (attributes.name) {
                extname  = path.extname(attributes.name);
            } else if (attributes.contentType) {
                // get extension by contentType
                extname = [...MEDIA_TYPES.entries()]
                    .filter(({ 1: v }) => v === attributes.contentType)
                    .map(([k]) => k)[0];
            }
            if (extname == null) {
                return reject(new ArgumentError('Either name or contentType is empty or the specified content type is not acceptable.'));
            }
            // eslint-disable-next-line no-unused-vars
            return tmp.file({ postfix: extname, discardDescriptor: true }, (err, file, fd, cleanupCallback) => {
                if (err) {
                    return reject(err);
                }
                return fs.writeFile(file, finalData, err => {
                    if (err) {
                        return reject(err);
                    }
                    self.replace(context, file, attributes).then( result => {
                        // cleanup
                        cleanupCallback();
                        // return data
                        return resolve(result);
                    }).catch (err => {
                        return reject(err);
                    });
                });
            });
        });
    }

    /**
     * Returns a content type based on the given extension name
     * @param {string} extname 
     */
    getContentType(extname) {
        return MEDIA_TYPES.get(extname);
    }

}

export {
    DocumentNumberService
}