import {HttpBadRequestError, HttpNotFoundError, HttpServerError, TraceUtils} from "@themost/common";
import {EncryptionStrategy} from "@themost/web";
import DocumentNumberSeriesItem from "./models/DocumentNumberSeriesItem";
// eslint-disable-next-line no-unused-vars
const {ExpressDataApplication} = require('@themost/express');
import {Router} from 'express';
import path from 'path';
import util from 'util';
import * as el from '../locales/el.json';
import * as en from '../locales/en.json';
import i18n from "i18n";
/**
 *
 * @param {ExpressDataApplication} app
 * @returns {*}
 */
function documentNumberVerifier(app) {

    const translateService = app.getService(function TranslateService() {
    });
    translateService.setTranslation('el', el);
    translateService.setTranslation('en', en);
    const router = Router();
    // use this handler to create a router context
    router.use((req, res, next) => {
        // create router context
        const newContext = app.createContext();
        /**
         * try to find if request has already a data context
         * @type {ExpressDataContext|*}
         */
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                // finalize interactive context
                return interactiveContext.finalize(() => {
                    // and assign new context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: () => {
                            return newContext
                        }
                    });
                    // exit handler
                    return next();
                });
            }
        }
        // otherwise assign context
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
                return newContext
            }
        });
        // and exit handler
        return next();
    });
    router.use((req, res, next) => {
        // set context locale from request
        req.context.locale  = req.locale;
        // set translation
        req.context.translate = function() {
            return i18n.__.apply(req.context, Array.from(arguments));
        };
        return next();
    });
    // use this handler to finalize router context
    // important note: context finalization is very important in order
    // to close and finalize database connections, cache connections etc.
    router.use((req, res, next) => {
        req.on('end', () => {
            //on end
            if (req.context) {
                //finalize data context
                return req.context.finalize( () => {
                    //
                });
            }
        });
        return next();
    });

    router.get('/verify',
        /**
         * @param {Request} req
         * @param {Response} res
         * @param {*} next
         * @returns {Promise<*>}
         */
        async function documentVerify(req, res, next) {
        if (req.query.code == null) {
            // render with 400 Bad Request
            return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpBadRequestError('Document code is missing.')
            });
        }
        // decode param
        let codeParams;
        try {
            const codeParamsString = req.context.getApplication().getService(EncryptionStrategy).decrypt(req.query.code);
            codeParams = JSON.parse(codeParamsString);
        } catch (err) {
            TraceUtils.error('(DocumentNumberValidator) An error occurred while decrypting document code.');
            TraceUtils.error(err);
            // render with 400 Bad Request
            return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpBadRequestError('Bad document code format.')
            });
        }
        // search for document
        const item = await req.context.model(DocumentNumberSeriesItem)
            .where('parentDocumentSeries').equal(codeParams.documentSeries)
            .and('documentNumber').equal(codeParams.documentNumber)
            .silent()
            .getItem();
        if (item == null) {
            // render with 404 Not Found
            return res.status(404).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpNotFoundError()
            });
        }
        // get content service
        // noinspection JSCheckFunctionSignatures
        const service =  req.context.application.getService(function PrivateContentService() {
        });
        if (service == null) {
            // render with 400 Bad Request
            return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpServerError('Content service cannot be found or is inaccessible.')
            });
        }
        // get physical path
        const resolvePhysicalPath = util.promisify(service.resolvePhysicalPath).bind(service);
        const sendFile = util.promisify(res.sendFile).bind(res);
        // send file
        try {
            //get physical file path
            const physicalPath = await resolvePhysicalPath(req.context, item);
            // send file
            res.contentType(item.contentType);
            await sendFile(physicalPath);
        }
        catch (err) {
            // if resolved file cannot be found in the specified path
            if (err.code === 'ENOENT') {
                TraceUtils.error(`An error occurred while trying to resolve physical path for ${item.name}.`);
                TraceUtils.error(err);
                // throw not found error
                return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                    ok: false,
                    html: {
                        context: req.context
                    },
                    error: new HttpNotFoundError('Not Found')
                });
            }
            // otherwise throw error
            return next(err);
        }

    });
    return router;
}

export {
    documentNumberVerifier
}
