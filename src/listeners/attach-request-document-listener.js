import {DataError} from "@themost/common";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context  = event.model.context;
    // get previous state of published attribute
    let previousPublished = false;
    if (event.state === 2) {
        if (event.previous == null) {
            throw new DataError('E_STATE', 'Current item previous state cannot be found or is inaccessible.', event.model.name);
        }
        previousPublished = event.previous.published;
    }
    // get original published attribute
    const published = await event.model.where('id').equal(event.target.id).select('published').silent().value();
    const RequestDocumentActions = context.model('RequestDocumentAction');
    if (RequestDocumentActions == null) {
        return;
    }
    let action;
    if (published === true && previousPublished === false) {
        // search for request document action with result
        action = await RequestDocumentActions.where('result').equal(event.target.id).silent().getTypedItem();
        if (action && action.actionStatus.alternateName === 'CompletedActionStatus') {
            // add message
            const newMessage = await context.model('StudentMessage').silent().save({
                student: action.student,
                action: action.id,
                subject: context.__('CompletedStudentRequestSubject'),
                body: context.__('AttachmentFileCreated')
            });
            // get typed message
            const message = context.model('StudentMessage').convert(newMessage);
            // insert attachment
            await message.property('attachments').silent().insert(event.target);
            // change owner
            const user = await context.model('Student').find(action.student).select('user').silent().value();
            if (user) {
                await event.model.silent().save({
                    id : event.target.id,
                    owner: user
                });
            }
        }
    } else if (published === false && previousPublished === true) {
        action = await RequestDocumentActions
            .where('result').equal(event.target.id)
            .equal(event.target.id)
            .expand({
                name: 'messages',
                options: {
                    $expand: 'attachments'
                }
            })
            .silent()
            .getTypedItem();
        if (action && action.actionStatus.alternateName === 'CompletedActionStatus') {
            // find message
            let messageAttachment;
            let actionMessage;
            for (let i = 0; i < action.messages.length; i++) {
                actionMessage = action.messages[i];
                messageAttachment = actionMessage.attachments.find( (item) => {
                   return item.id === event.target.id;
                });
                if (messageAttachment) {
                    break;
                }
            }
            // if attachment found
            if (messageAttachment) {
                const message = context.model('StudentMessage').convert(actionMessage);
                // remove attachment
                await message.property('attachments').silent().remove(event.target);
                // remove message
                await context.model('StudentMessage').remove(message);
                // change owner
                const user = await context.model('Student').find(action.student).select('user').silent().value();
                if (user) {
                    await event.model.silent().save({
                        id : event.target.id,
                        owner: null
                    });
                }
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
