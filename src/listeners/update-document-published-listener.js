/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    const context  = event.model.context;
    if (Object.prototype.hasOwnProperty.call(event.target, 'documentStatus') && event.target.documentStatus != null) {
        const documentStatus = await context.model('DocumentStatusType').find(event.target.documentStatus).getItem();
        if (documentStatus != null && documentStatus.alternateName === 'PublishedActionStatus') {
            // set published
            event.target.published = true;
            event.target.datePublished = new Date();
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}