import {ApplicationService, IApplication} from '@themost/common';

declare interface VerifyCodeParams {
    documentSeries?: any;
    documentNumber?: any
}

declare class DocumentNumberVerifierService extends ApplicationService  {
    constructor(app: IApplication);

    generateURI(documentSeries: any, documentNumber: any): string;

}

export {
    VerifyCodeParams,
    DocumentNumberVerifierService
}
