import * as DocumentNumberSeries from './models/DocumentNumberSeries';
import * as DocumentNumberSeriesItem from './models/DocumentNumberSeriesItem';
import * as DepartmentDocumentNumberSeriesItem from './models/DepartmentDocumentNumberSeries';
export * from './DocumentNumberService';
export * from './DefaultDocumentNumberService';
export {DocumentNumberSeries,
    DocumentNumberSeriesItem,
    DepartmentDocumentNumberSeriesItem};
export * from './DocumentNumberVerifierService';
export declare const MEDIA_TYPES: Map<string, string>;
