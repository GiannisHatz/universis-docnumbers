import {IApplication} from '@themost/common';
import { DocumentNumberService } from './DocumentNumberService';
export declare abstract class DefaultDocumentNumberService extends DocumentNumberService {
    constructor(app: IApplication);
}