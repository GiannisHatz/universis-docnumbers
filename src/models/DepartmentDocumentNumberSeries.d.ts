import {EdmMapping, EdmType, DataObject} from '@themost/data';
import * as DocumentNumberSeries from './DocumentNumberSeries';

/**
 * @class
 */
declare class DepartmentDocumentNumberSeries extends DocumentNumberSeries {

}

export = DepartmentDocumentNumberSeries;