import {EdmMapping, EdmType, DataObject} from '@themost/data';

export declare interface DocumentNumberFormatOptions {
     currentDate?: Date,
     academicYear?: any
}

/**
 * @class
 */
declare class DocumentNumberSeries extends DataObject {

     public id: number; 
     public identifier?: number; 
     public additionalType?: string; 
     public alternateName?: string; 
     public description?: string; 
     public image?: string; 
     public name?: string; 
     public url?: string; 
     public dateCreated?: Date; 
     public dateModified?: Date; 
     public createdBy?: any; 
     public modifiedBy?: any; 

     async next(): any;
     async format(index: any, format?: DocumentNumberFormatOptions): string;

}

export = DocumentNumberSeries;