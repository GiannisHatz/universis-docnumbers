/// <reference path="./DocumentNumberSeries.d.ts" />
import {Args, DataError} from '@themost/common';
import {DataObject, EdmMapping} from '@themost/data';
import numeral from 'numeral';
import moment from 'moment';

@EdmMapping.entityType('DocumentNumberSeries')
class DocumentNumberSeries extends DataObject {
    async next() {
        // validate id
        Args.notNumber(this.id, 'Item identifier');
        // get id
        const id = this.id;
        // get number format if empty
        const model = this.context.model(DocumentNumberSeries);
        let lastIndex;
        await new Promise((resolve, reject) => {
            this.context.db.executeInTransaction( (cb) => {
                // execute async method
               (async function() {
                   /**
                    * @type {{lastIndex: any, useParentIndex: boolean, parent: any, parentLastIndex: any}}
                    */
                   const item = await model.where('id').equal(id)
                        .select('lastIndex', 'useParentIndex', 
                        'parent/id as parent',
                        'parent/lastIndex as parentLastIndex').silent().getItem();
                   if (item.useParentIndex) {
                       // validate if parent is null
                       if (item.parent == null) {
                           // and throw exception
                           throw new DataError('E_FOUND', 'Parent document number series item cannot be found or is inaccessible', null, 'DocumentNumberSeries', 'parent');
                       }
                       // get parent last index
                       lastIndex = item.parentLastIndex;
                   } else {
                       // otherwise use item lastIndex
                    lastIndex = item.lastIndex;
                   }
                   // increase last index
                   lastIndex += 1;
                   // and save
                   if (item.useParentIndex) {
                       // update parent.lastIndex and continue
                       await model.silent().save({
                           id: item.parent,
                           lastIndex: lastIndex
                       });
                   } else {
                       // update lastIndex
                       await model.silent().save({
                           id,
                           lastIndex
                       });
                   }
               })().then( () => {
                    return cb();
               }).catch( err => {
                    return cb(err);
               });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        });
      return lastIndex;
    }
    /**
     * 
     * @param {*} index 
     * @param {DocumentNumberFormatOptions=} formatOptions 
     * @return {string}
     */
    async format(index, formatOptions) {
        // get format params
        const options = Object.assign({ 
            currentDate: new Date()
         }, formatOptions);
         // validate parent numberFormat
         if (Object.prototype.hasOwnProperty.call(this, 'useParentIndex') === false) {
             this.useParentIndex = await this.context.model(this.getType())
                .where('id').equal(this.getId())
                .select('useParentIndex').silent().value();
         }
         if (this.useParentIndex) {
            // get parent
            this.parent = await this.property('parent').silent().getItem();
            if (this.parent == null) {
                throw new DataError('E_FOUND', 'Parent document number series item cannot be found or is inaccessible', null, 'DocumentNumberSeries', 'parent');
            }
            this.numberFormat = this.parent.numberFormat;
         }
         // fetch numberFormat if empty
        if (this.numberFormat == null) {
            this.numberFormat = await this.context.model(this.getType())
                .where('id').equal(this.getId())
                .select('useParentIndex').silent().value();
        }
        Args.notNull(this.numberFormat, 'Document number format');
        let result = this.numberFormat;
        // first find number parameter e.g. {0000} or {0}
        let match = /\{(\d+)\}/.exec(result);
        if (match) {
            // e.g. match[0] => {0000} and match[1] => 0
            const formattedIndex = numeral(index).format(match[1]);
            // replace formatted index
            result = result.replace(/\{(\d+)\}/, formattedIndex);
        }
        else {
            // throw error for invalid expression
            throw new Error('Invalid document number format expression. Index parameter is missing');
        }
        if (formatOptions.academicYear) {
            const academicYear = this.context.model('AcademicYear').convert(formatOptions.academicYear).getId();
            if (Number.isInteger(academicYear) === false) {
                throw new DataError('E_VALUE', 'Invalid academic year. Expected number.', null, 'AcademicYear', 'id');
            }
            if (/{AAAAAA}/g.test(result)) {
                // replace {AAAAAA} with academicYear long format e.g 2019-2020
                result = result.replace(/{AAAAA}/i, `${academicYear}-${academicYear+1}`);
            }
            if (/{AAAAA}/g.test(result)) {
                // replace {AAAAA} with academicYear alternate name e.g 2019-20
                result = result.replace(/{AAAAA}/i, `${academicYear}-${(academicYear+1).toString().substr(2)}`);
            }
            if (/{AAAA}/g.test(result)) {
                // replace {AAAA} with academic year e.g 2019
                result = result.replace(/{AAAA}/i, academicYear);
            }
        }

        // format date parameters if any
        const re = /\{(.*?)\}/i;
        match = re.exec(result);
        let momentInstance = moment(options.currentDate);
        while(match) {
            // e.g. match[0] => {YYYY} and match[1] => YYYY
            let replaceMatch = momentInstance.format(match[1]);
            result = result.replace(match[0], replaceMatch);
            match = re.exec(result);
        }
        return result;
    }
    /**
     * Gets next index and format document number
     * @param {DocumentNumberFormatOptions=} formatOptions 
     * @return {string}
     */
    async nextAndFormat(formatOptions) {
        const nextIndex = await this.next();
        return this.format(nextIndex, formatOptions);
    }
}
module.exports = DocumentNumberSeries;