# @universis/docnumbers

Universis api server default document numbering service

## Installation

npm i @universis/docnumbers

## Usage

Register `DocumentNumberService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/docnumbers#DocumentNumberService",
            "strategyType": "@universis/docnumbers#DefaultDocumentNumberService"
        }
    ]

Add `DocumentNumberSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/docnumbers#DocumentNumberSchemaLoader"
                    }
                ]
            }
        }
    }